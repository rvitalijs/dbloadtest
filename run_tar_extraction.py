# -*- coding: utf-8 -*-

import os
import sys
import logging

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from core import LogExtension
from core.helpers.file_helper import FileHelper


def main():
	LogExtension.logger().info('File processing is started')
	tar_path = r'D:\AccessLog\tar1'
	tar_processed_path = r'D:\AccessLog\tar_processed'
	access_log_path = r'D:\AccessLog\src'

	file_list = FileHelper.get_file_list(tar_path, '^(\d{8})_(\d{6})_(\S+)(\.tar\.gz)$')

	for tar_file in file_list:
		try:
			tar_file_name = os.path.basename(tar_file)
			extracted_file_prefix = tar_file_name[0:16]
			FileHelper.extract_tar_gz_file('%s/%s' % (tar_path, tar_file), access_log_path, extracted_file_prefix, '^(\S+)(access)(\S*)$')
			os.rename('%s/%s' % (tar_path, tar_file), '%s/%s' % (tar_processed_path, tar_file))
		except:
			LogExtension.log_trace_back()
			print 'Exception extracting files from tar: %s' % tar_file

	exit(1)

if __name__ == "__main__":
	log_file_name = '%s/%s.%s' % \
		(os.path.dirname(os.path.abspath(__file__)), os.path.basename(__file__).split('.')[:-1][0], 'log')
	logging.basicConfig(
		level=logging.DEBUG,
		filename=log_file_name,
		filemode='a', datefmt='%Y-%m-%d %H:%M:%S',
		format='%(asctime)-15s> %(name)s %(levelname)s %(message)s')
	main()
