# -*- coding: utf-8 -*-

import logging
import os
import sys
import time
from time import sleep
from datetime import datetime
from multiprocessing import Process, Queue

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from core.helpers.file_helper import FileHelper
from db.db_handler import CSVFile
from core.helpers.access_log import AccessLog
from core import LogExtension


def execute(queue, results_queue, worker_id, access_log_path, csv_path, starting_row_no_for_pk):
	total_row_count = 0
	out_file_name = '%s/%s_%d_access.csv' % (csv_path, datetime.now().strftime("%Y%m%d_%H%M%S"), worker_id)
	csv_file = CSVFile(worker_id, 0, out_file_name)
	csv_file.connect()
	while True:
		if queue.qsize() > 0:
			fn = queue.get()

			if fn == 'quit':
				break

			file_name = '%s/%s' % (access_log_path, fn)
			row_count = 0

			f = open(file_name, "r")
			for line in f:
				try:
					line_inf = AccessLog.parse_apache_log_line(line)
					if line_inf is None:
						LogExtension.logger().info('DB Worker(%d): Unrecognized access log line %s in file %s' % (worker_id, line.replace('\n', ''), fn))
					else:
						row_count += 1
						row = AccessLog.anonymize_data(line_inf)
						csv_file.insert_row([starting_row_no_for_pk+total_row_count]+row)
						total_row_count += 1
				except:
					LogExtension.log_trace_back()
					LogExtension.logger().info('DB Worker(%d): exception on line %s while processing file %s' % (worker_id, line.replace('\n', ''), fn))

			results_queue.put([worker_id, fn, row_count, total_row_count])
			print "%s> DB Worker(%d): file %s has been processed. Row count un file: %d. Total row count in csv: %d" % (
				datetime.now().strftime("%Y-%m-%d %H:%M:%S"), worker_id, fn, row_count, total_row_count)

	csv_file.close()
	print "%s> DB Worker(%d): has been terminated. Bye bye! " % (datetime.now().strftime("%Y-%m-%d %H:%M:%S"), worker_id)


def build_worker_pool(queue, results_queue, access_log_path, csv_path, worker_count, starting_row_no_for_pk):
	workers = []
	for worker_id in range(worker_count):
		worker = Process(target=execute, args=(queue, results_queue, worker_id, access_log_path, csv_path, starting_row_no_for_pk+10000000*worker_id))
		workers.append(worker)
		worker.start()
	return workers


def main():
	prm = get_args()
	start_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	print "%s> Go" % start_time

	queue = Queue()
	results_queue = Queue()

	worker_threads = build_worker_pool(
		queue, results_queue, prm['access_log_path'], prm['csv_path'], prm['worker_count'], prm['starting_row_no_for_pk'])

	# Add the urls to process
	file_list = FileHelper.get_file_list(prm['access_log_path'], '^(\S+)(access)(\S*)$', False)
	for url in file_list:
		queue.put(url)

	for worker in worker_threads:
		queue.put('quit')

	while queue.qsize() > 0:
		sleep(2)

	while results_queue.qsize() > 0:
		res = results_queue.get()
		print "%s> DB Worker(%d): file %s has been processed. Row count un file: %d. Total row count in csv: %d" % (
			datetime.now().strftime("%Y-%m-%d %H:%M:%S"), res[0], res[1], res[2], res[3])

	print "Main thread has been completed. Start time: %s, End time: %s" % (start_time, datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


def get_args():
	try:
		if len(sys.argv) == 1:
			print_usage()
			exit(1)

		access_log_path = sys.argv[1]
		if access_log_path is None or access_log_path == '' or not os.path.isdir(access_log_path):
			raise ValueError('{access_log_path}: Value "%s" is incorrect or path does not exist.' % access_log_path)

		csv_path = sys.argv[2]
		if csv_path is None or csv_path == '' or not os.path.isdir(csv_path):
			raise ValueError('{csv_path}: Value "%s" is incorrect or path does not exist.' % csv_path)

		worker_count = int(sys.argv[3])
		if worker_count < 1 or worker_count > 100:
			raise ValueError('{worker_count}: Value %d is incorrect. {worker_count} can take values only between 1 and 100.' % worker_count)

		starting_row_no_for_pk = int(sys.argv[4])
		if starting_row_no_for_pk < 1:
			raise ValueError('Value %d is incorrect. {start_row} can be > 1.' % starting_row_no_for_pk)

		print '%s> Settings: \n\t{access_log_path}: "%s", \n\t{csv_path}: "%s"\n {starting_row_no_for_pk}' % (
				datetime.now().strftime("%Y-%m-%d %H:%M:%S"), access_log_path, csv_path)
		sleep(2)

		return {'access_log_path': access_log_path, 'csv_path': csv_path, 'worker_count': worker_count, 'starting_row_no_for_pk': starting_row_no_for_pk}

	except Exception as ex:
		print "\nArgument error: %s. %s\n" % (ex.__class__.__name__, str(ex))
		print_usage()
		exit(1)


def print_usage():
	usage_str = 'python run_batch_insert.pyc {access_log_path} {csv_path} {worker_count} {starting_row_no_for_pk}'
	example_str = 'python run_batch_insert.pyc "\\tmp\\access_logs\\" "\\tmp\\access_logs_csv\\" 2'
	usage_restrictions = \
		'\t{access_log_path} path where access log files are stored; \n' \
		'\t{csv_path} path where to store resulting csv files; \n' \
		'\t{worker_count} how many sub processes will process access log files. \n\t\t' \
		'On this parameter depends how many resulting csv files will be created. One file per worker; \n' \
		'\t{starting_row_no_for_pk} starting row number.'

	print 'Usage:\n%s\nExample:\n%s\nRestrictions:\n%s' % (usage_str, example_str, usage_restrictions)


if __name__ == "__main__":
	log_file_name = '%s/%s.%s' % \
		(os.path.dirname(os.path.abspath(__file__)), os.path.basename(__file__).split('.')[:-1][0], 'log')
	logging.basicConfig(
		level=logging.DEBUG,
		filename=log_file_name,
		filemode='a', datefmt='%Y-%m-%d %H:%M:%S',
		format='%(asctime)-15s> %(name)s %(levelname)s %(message)s')
	main()
