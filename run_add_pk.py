# -*- coding: utf-8 -*-

import logging
import os
import sys
from datetime import datetime
import csv
from multiprocessing import Process, Queue
from time import sleep, time

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from db.db_handler import CSVFile


def execute(data_queue, stop_queue, worker_id, worker_debug_info, output_file_name):
	batch_cnt = 0
	total_rows = 0
	# , delimiter=',', quoting=csv.QUOTE_ALL, quotechar='"', escapechar=''
	csv_file = CSVFile(worker_id, 0, output_file_name)
	csv_file.connect()
	while True:
		if data_queue.qsize() > 0:
			rows = data_queue.get()

			if len(rows) == 0:
				break

			batch_cnt += 1

			csv_file.insert_access_log_rows(rows)
			total_rows += len(rows)
			del rows[:]

			print "Worker(%d) batch no %d, total_rows: %d" % (worker_id, batch_cnt, total_rows)

		else:
			if worker_debug_info == 1:
				print 'Worker(%d) sleeps next 0.1 secs.' % worker_id
			sleep(0.1)
	csv_file.close()
	stop_queue.put('Worker(%d) has been terminated. batch no %d, total_rows: %d' % (worker_id, batch_cnt, total_rows))


def read_csv(csv_file, batch_size, start_row):
	rows = []
	f = open(csv_file, "rb")
	csv_reader = csv.reader(f, delimiter=',', quotechar='"')
	row_no = start_row
	for row in csv_reader:
		row_no += 1
		rows.append([row_no]+row)
		if len(rows) == batch_size:
			yield rows
			rows = []

	if len(rows) > 0:
		yield rows

	f.close()


def execute_reader(data_queue, csv_file_name, start_row, batch_size, reader_buffer_size, reader_sleep_time, reader_debug_info):
	batch_cnt = 0
	total_rows = 0
	csv_iterator = read_csv(csv_file_name, batch_size, start_row)
	start_time = int(round(time() * 1000))
	while True:
		try:
			if data_queue.qsize() < reader_buffer_size:
				rows = csv_iterator.next()
				total_rows += len(rows)
				data_queue.put(rows)
				batch_cnt += 1
				if reader_debug_info == 1:
					print 'Reader has read %d rows in batches: %d in %.3f sec. queue.qsize: %d' % (total_rows, batch_cnt, float(int(round(time() * 1000)) - start_time)/1000, data_queue.qsize())
			else:
				if reader_sleep_time > 0:
					if reader_debug_info == 1:
						print 'Reader going to sleep %.3f seconds. batch_size: %d, batch_cnt: %d, queue.qsize: %d' % (reader_sleep_time, batch_size, batch_cnt, data_queue.qsize())
					sleep(reader_sleep_time)
		except StopIteration:
			os.remove(csv_file_name)
			print "CSV file %s has been successfully removed" % csv_file_name
			break

	data_queue.put([])
	print "Reader thread has been completed. Processed batch count is %d." % batch_cnt


def build_worker_pool(
		data_queue, stop_queue,
		csv_file_name,
		start_row, batch_size, worker_debug_info,
		reader_buffer_size, reader_sleep_time, reader_debug_info,
		csv_db_file_name):

	workers = []
	worker = Process(target=execute_reader, args=(data_queue, csv_file_name, start_row, batch_size, reader_buffer_size, reader_sleep_time, reader_debug_info))
	workers.append(worker)
	worker.start()

	worker = Process(target=execute, args=(data_queue, stop_queue, 0, worker_debug_info, csv_db_file_name))
	workers.append(worker)
	worker.start()

	return workers


def main():
	prm = get_args()

	start_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	print "%s> Go" % start_time

	queue = Queue()
	stop_queue = Queue()

	worker_threads = build_worker_pool(
		queue, stop_queue,
		prm['csv_file_name'],
		prm['start_row'], prm['batch_size'], prm['worker_debug_info'],
		prm['reader_buffer_size'], prm['reader_sleep_time'], prm['reader_debug_info'],
		prm['csv_db_file_name'])

	while stop_queue.qsize() == 0:
		sleep(5)

	print stop_queue.get()
	print "Main thread has been completed. Start time: %s, End time: %s" % (start_time, datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	exit(1)


def get_args():
	try:
		if len(sys.argv) == 1:
			print_usage()
			exit(1)

		csv_file_name = sys.argv[1]
		if csv_file_name is None or csv_file_name == '':
			raise ValueError('Value "%s" is incorrect. {csv_file_name} must be supplied.' % csv_file_name)

		if not os.path.isfile(csv_file_name):
			raise ValueError('File "%s" does not exist.' % csv_file_name)

		start_row = int(sys.argv[2])
		if start_row < 1:
			raise ValueError('Value %d is incorrect. {start_row} can be > 1.' % start_row)

		batch_size = int(sys.argv[3])
		if batch_size < 1 or batch_size > 1000:
			raise ValueError('Value %d is incorrect. {batch_size} can take values only between 1 and 1000.' % batch_size)

		worker_debug_info = int(sys.argv[4])
		if worker_debug_info < 0 or worker_debug_info > 1:
			raise ValueError('Value %d is incorrect. {worker_debug_info} only can take value 0 or 1.' % worker_debug_info)

		reader_buffer_size = int(sys.argv[5])
		if reader_buffer_size < 1 or worker_debug_info > 1000:
			raise ValueError('Value %d is incorrect. {reader_buffer_size} can take values only between 1 and 1000.' % reader_buffer_size)

		reader_sleep_time = float(sys.argv[6])
		if reader_sleep_time < 0 or reader_sleep_time > 9.9:
			raise ValueError('Value %.3f is incorrect. {reader_sleep_time} can take values only between 0 and 9.9 (sec.)' % reader_sleep_time)

		reader_debug_info = int(sys.argv[7])
		if reader_debug_info < 0 or reader_debug_info > 1:
			raise ValueError('Value %d is incorrect. {reader_debug_info} only can take value 0 or 1.' % reader_debug_info)

		csv_db_file_name = sys.argv[8]
		if csv_db_file_name is None or csv_db_file_name == '':
			raise ValueError('Value "%s" is incorrect. {csv_db_file_name} must be supplied.' % csv_db_file_name)

		print "%s> Settings: \n" \
			'\tcsv_file_name: "%s", \n' \
			'\tstart_row: %d, batch_size: %d, worker_debug_info: %d, \n' \
			'\treader_buffer_size: %d, reader_sleep_time: %.3f, reader_debug_info: %d, \n' \
			'\tcsv_db_file_name: "%s"\n' % (
				datetime.now().strftime("%Y-%m-%d %H:%M:%S"), csv_file_name,
				start_row, batch_size, worker_debug_info,
				reader_buffer_size, reader_sleep_time, reader_debug_info,
				csv_db_file_name)
		sleep(2)

		return {
			'csv_file_name': csv_file_name,
			'start_row': start_row, 'batch_size': batch_size, 'worker_debug_info': worker_debug_info,
			'reader_buffer_size': reader_buffer_size, 'reader_sleep_time': reader_sleep_time, 'reader_debug_info': reader_debug_info,
			'csv_db_file_name': csv_db_file_name}

	except Exception as ex:
		print "\nArgument error: %s. %s\n" % (ex.__class__.__name__, str(ex))
		print_usage()
		exit(1)


def print_usage():
	usage_str = \
		'python run_batch_insert.pyc {csv_file_name} \\\n' \
		'\t{start_row} {batch_size} {worker_debug_info} \\\n' \
		'\t{reader_buffer_size} {reader_sleep_time} {reader_debug_info} \\\n' \
		'\t{csv_db_file_name}'
	example_str = 'python run_batch_insert.pyc "\\tmp\\access_log.csv" 8 500 1 100 0.5 1 "\\tmp\\load_statistics.csv" 2 1 2 ""'

	usage_restrictions = \
		'\t{csv_file_name} must be supplied file path to the access log; \n' \
		'\t{start_row} can be > 1; \n' \
		'\t{batch_size} can take values only between 1 and 1000; \n' \
		'\t{worker_debug_info} only can take value 0 or 1; \n' \
		'\t{reader_buffer_size} can take values only between worker_count and 1000; \n' \
		'\t{reader_sleep_time} can take values only between 0 and 9.9 (sec.); \n' \
		'\t{reader_debug_info} only can take value 0 or 1; \n' \
		'\t{csv_db_file_name} must be supplied file path which will be used as destination csv file for write sub process.'

	print 'Usage:\n%s\nExample:\n%s\nRestrictions:\n%s' % (usage_str, example_str, usage_restrictions)

if __name__ == "__main__":
	log_file_name = '%s/%s.%s' % \
		(os.path.dirname(os.path.abspath(__file__)), os.path.basename(__file__).split('.')[:-1][0], 'log')
	logging.basicConfig(
		level=logging.DEBUG,
		filename=log_file_name,
		filemode='a', datefmt='%Y-%m-%d %H:%M:%S',
		format='%(asctime)-15s> %(name)s %(levelname)s %(message)s')
	main()
