# -*- coding: utf-8 -*-

import sys
import traceback
import logging


class LogExtension(object):
	@classmethod
	def logger(cls):
		name = '.'.join([__name__, cls.__name__])
		return logging.getLogger(name)

	@staticmethod
	def get_trace_back():
		exc_type, exc_value, exc_traceback = sys.exc_info()
		s = traceback.extract_tb(exc_traceback)
		return "(%s), %s  in file: %s method %s at call %s on line %s" % (str(exc_value), exc_value.__class__, s[0][0], s[0][2], s[0][3], s[0][1])

	@classmethod
	def log_trace_back(cls, err=None):
		s = cls.get_trace_back()
		if err is None:
			print >> sys.stderr, s
			cls.logger().debug(s)
		else:
			print >> sys.stderr, "%s\n%s" % (s, err)
			cls.logger().debug("%s\n%s" % (s, err))