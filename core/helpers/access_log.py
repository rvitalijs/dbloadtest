# -*- coding: utf-8 -*-

import re
import sys
from datetime import datetime
from hashlib import md5


class AccessLog(object):
	@staticmethod
	def parse_apache_log_line(line):
		matches = re.search('^(\S+) (\S+) (\S+) \[([\w:/]+\s[+\-]\d{4})\] "(\S+) (\S+) (\S+)" (\d{3}) (\S+) "((?:[^”]|”)+)" "((?:[^”]|”)+)"$', line)
		if matches is not None:
			return AccessLog.get_log_line_data(matches, [1, 2, 3, 4, 5, 6, 7, 10, 11, 8, 9])

		matches = re.search('^(\S+) (\S+) (\S+) \[([\w:/]+\s[+\-]\d{4})\] "(\S+)" (\d{3}) (\S+) "((?:[^”]|”)+)" "((?:[^”]|”)+)"$', line)
		if matches is not None:
			return AccessLog.get_log_line_data(matches, [1, 2, 3, 4, 5, None, None, 8, 9, 6, 7])

		matches = re.search('^(\S+) (\S+) (\S+) \[([\w:/]+\s[+\-]\d{4})\] "(\S+) (\S+) (\S+)" (\d{3}) (\S+)$', line)
		if matches is not None:
			return AccessLog.get_log_line_data(matches, [1, 2, 3, 4, 5, 6, 7, None, None, 8, 9])

		matches = re.search('^(\S+) (\S+) (\S+) \[([\w:/]+\s[+\-]\d{4})\] "(\S+)" (\d{3}) (\S+)$', line)
		if matches is not None:
			return AccessLog.get_log_line_data(matches, [1, 2, 3, 4, 5, None, None, None, None, 6, 7])
		return None

	@staticmethod
	def get_line_data_values(pos, val):
		if val is None or val == '-':
			return None

		max_val_len = [20, 50, 50, 22, 10, 255, 50, 255, 255, 10, 10]

		if pos == 3:
			date_obj = datetime.strptime(val[0:20], '%d/%b/%Y:%H:%M:%S')
			return [
				date_obj.strftime('%Y-%m-%d'),
				int((date_obj - datetime(1970, 1, 1)).total_seconds()),
				int((date_obj - datetime(date_obj.year, date_obj.month, date_obj.day, 0, 0)).total_seconds())]

		return val[0:max_val_len[pos]]

	@staticmethod
	def get_log_line_data(matches, pattern):
		line_data = []
		pos = 0
		for p in pattern:
			if p is None:
				line_data.append(None)
			else:
				val = AccessLog.get_line_data_values(pos, matches.group(p))
				if pos == 3:
					line_data.append(val[0])
					line_data.append(val[1])
					line_data.append(val[2])
				else:
					line_data.append(val)
			pos += 1
		return line_data

	@staticmethod
	def anonymize_data(row_data):
		for i in range(11):
			if row_data[i] is not None:
				if i in [0, 1, 2, 7, 9]:
					row_data[i] = "%s" % md5(row_data[i].replace('"', '').replace(',', ';')).hexdigest()
				elif i in [3, 6, 8, 10]:
					row_data[i] = "%s" % row_data[i].replace('"', '').replace(',', ';')
		return row_data
