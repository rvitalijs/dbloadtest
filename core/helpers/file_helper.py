# -*- coding: utf-8 -*-

import shutil
import os
import tarfile
import re

from core import LogExtension


class FileHelper(LogExtension):
	RES_SUCCESS = 1
	RES_MORE_THAN_ONE_FILE_IN_ZIP = 2
	RES_SYSTEM_ERROR = 3

	@staticmethod
	def convert_path(path):
		if os.path.sep == '/':
			return path.replace('\\', os.path.sep)
		else:
			return path.replace('/', os.path.sep)

	@staticmethod
	def add_last_sep2path(path):
		if path[:-1] != os.path.sep:
			return path + os.path.sep
		return path

	@staticmethod
	def read_file_contents(file_path, file_open_mode="r"):
		fh = open(file_path, file_open_mode)
		contents = fh.read()
		fh.close()
		return contents

	@staticmethod
	def is_only_filename(path):
		if os.path.basename(path) == path:
			return True
		return False

	@staticmethod
	def get_path(path):
		return os.path.dirname(path)

	@staticmethod
	def remove_dir(dir_path):
		try:
			shutil.rmtree(dir_path)
			return True
		except:
			FileHelper.log_trace_back()
		return False

	@staticmethod
	def remove_file(file_path):
		try:
			os.remove(file_path)
			return True
		except:
			FileHelper.log_trace_back()
		return False

	@staticmethod
	def file_exists(filename):
		return os.path.isfile(filename)

	@staticmethod
	def get_file_list(dir_name, filter_by_regexp=None, abs_path=False):
		file_list = []
		for item_name in os.listdir(dir_name):
			if os.path.isfile('%s/%s' % (dir_name, item_name)):
				if filter_by_regexp is not None and re.match(filter_by_regexp, item_name) is None:
					continue
				file_list.append('%s/%s' % (dir_name, item_name) if abs_path else item_name)
			else:
				file_list += ['%s/%s' % (item_name, file_name) for file_name in FileHelper.get_file_list('%s/%s' % (dir_name, item_name))]
		return file_list

	@staticmethod
	def extract_tar_gz_file(tar_file_name, destination, extracted_file_prefix='', filter_by_regexp=None):

		destination_tmp = '%s/tmp-tar-extraction-dir' % destination

		if not os.path.exists(destination_tmp):
			os.makedirs(destination_tmp)

		if extracted_file_prefix is None:
			extracted_file_prefix = ''

		with tarfile.open(tar_file_name, "r|*") as tar:
			member_list = []
			for member in tar:
				if member.isfile():
					filename = os.path.basename(member.name)

					if filter_by_regexp is not None and re.match(filter_by_regexp, filename) is None:
						continue
					else:
						member_list.append(member)
			tar.close()

		with tarfile.open(tar_file_name, "r|*") as tar:
			tar.extractall(path=destination_tmp, members=member_list)
			tar.close()

		for file_name in FileHelper.get_file_list(destination_tmp):
			filename = os.path.basename(file_name)
			os.rename('%s/%s' % (destination_tmp, file_name), '%s/%s%s' % (destination, extracted_file_prefix, filename))

		FileHelper.remove_dir(destination_tmp)
