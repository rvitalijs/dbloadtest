# -*- coding: utf-8 -*-

BIT = 1
BOOL = 2
INT = 3
FLOAT = 4
DECIMAL = 5
VARCHAR = 6		# UTF8 strings
NVARCHAR = 7 	# Unicode strings
DATE = 8
DATETIME = 9
