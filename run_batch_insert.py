# -*- coding: utf-8 -*-

import logging
import os
import sys
from datetime import datetime
import csv
from multiprocessing import Process, Queue
from time import sleep, time

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from db.db_handler import DBHandler, CSVFile
from db import CSV_DB


def execute(data_queue, resp_queue, pause_workers_queue, worker_id, db_type, output_file_name):
	db_handler = DBHandler(worker_id, 0, db_type, output_file_name)
	db_handler.db().connect()
	db_handler.db().autocommit(True)
	# rows_cnt = 0
	while True:
		if pause_workers_queue.qsize() == 0:
			data = data_queue.get()
			if len(data[1]) == 0:
				break
			try:
				start_time = int(round(time() * 1000))
				db_handler.db().insert_access_log_rows(data[1])
				end_time = int(round(time() * 1000))
				resp_ms = end_time - start_time
				resp_queue.put([start_time, end_time, len(data[1]), 1 if resp_ms == 0 else resp_ms, data[0]])
				# rows_cnt += len(data[1])
				# if rows_cnt > 5000 and db_type == 6:
				# 	db_handler.db().commit()
				# 	rows_cnt = 0

				if data[0] == 1:
					print 'Worker(%d): has been read last batch of size %d. data_queue.qsize: %d. All workers will be paused until all batches of new size will be read into memory.' % (
						worker_id, len(data[1]), data_queue.qsize())
					sleep(2)
					pause_workers_queue.put(1)
			except Exception as ex:
				print "Worker(%d). Exception(%s) raised: %s" % (worker_id, ex.__class__.__name__, str(ex))
		else:
			sleep(0.1)

	# db_handler.db().commit()
	db_handler.db().close()
	resp_queue.put([0, worker_id])


def execute_aggregate(delay_ms, resp_queue, stats_queue, pause_reader_queue, worker_count):
	start_time = None
	batch_cnt = 0
	rows_cnt = 0
	max_time = None
	min_time = None
	total_resp_ms = 0
	batch_speed = []
	resp_times = []
	stopped_worker_cnt = 0
	while True:
		if resp_queue.qsize() > 0:
			resp = resp_queue.get()

			if resp[0] > 0:
				batch_cnt += 1
				if min_time is None or min_time > resp[0]:
					min_time = resp[0]
				if max_time is None or max_time < resp[1]:
					max_time = resp[1]

				rows_cnt += resp[2]
				total_resp_ms += resp[3]
				batch_speed.append(int(round(float(resp[2]) / (float(resp[3]) / 1000))))
				resp_times.append(resp[3])

				if start_time is None:
					start_time = resp[0]

				time_delta = int(round(time() * 1000)) - start_time
				if time_delta > delay_ms or resp[4] == 1:
					med_idx = len(batch_speed) / 2
					stats_queue.put([
						resp[2],
						min_time, max_time, time_delta if resp[4] == 0 else (max_time - min_time), batch_cnt, rows_cnt, total_resp_ms,
						min(resp_times), max(resp_times), resp_times[med_idx],
						min(batch_speed), max(batch_speed), batch_speed[med_idx], sum(batch_speed)])
					batch_cnt = 0
					rows_cnt = 0
					max_time = None
					min_time = None
					total_resp_ms = 0
					del batch_speed[:]
					del resp_times[:]
					start_time = int(round(time() * 1000))
					# stop waiting
					if resp[4] == 1:
						start_time = None
						print 'Resuming reader after 2 sec.'
						sleep(2)
						s = pause_reader_queue.get()
			else:
				stopped_worker_cnt += 1
				print "Worker(%d): is terminated." % resp[1]

				if stopped_worker_cnt >= worker_count:
					break
		else:
			sleep(0.05)
	stats_queue.put([0])


def get_zero_counters():
	return {
		'batch_cnt': 0, 'rows_cnt': 0,
		'min_time_ms': None, 'max_time_ms': None, 'elapsed_time': 0,
		'avg_resp_ms': 0, 'min_resp_ms': None, 'max_resp_ms': None, 'mid_resp_ms': 0, 'total_resp_ms': 0,
		'min_batch_speed': None, 'max_batch_speed': None, 'mid_batch_speed': 0,
		'avg_rows_per_sec': 0, 'min_rows_per_sec': 0, 'max_rows_per_sec': 0, 'mid_rows_per_sec': 0,
		'min_rows_per_sec2': 0, 'max_rows_per_sec2': 0, 'mid_rows_per_sec2': 0,
		'resp_time_medians': [], 'speed_medians': [], 'speeds_per_measure': [], 'batch_speed_medians': [],
	}


def upd_counters(c, stat):
	# stats_queue.put([
	# 	0 - resp[2],
	# 	1 - min_time, 2 - max_time, 3 - var_delta,
	# 	4 - batch_cnt, 5 - rows_cnt, 6 - total_resp_ms,
	# 	7 - min(resp_times), 8 - max(resp_times), 9 - resp_times[med_idx],
	# 	10 - min(batch_speed), 11 - max(batch_speed), 12 - batch_speed[med_idx]])
	c['batch_cnt'] += stat[4]
	c['rows_cnt'] += stat[5]

	if c['min_time_ms'] is None or c['min_time_ms'] > stat[1]:
		c['min_time_ms'] = stat[1]
	if c['max_time_ms'] is None or c['max_time_ms'] < stat[2]:
		c['max_time_ms'] = stat[2]

	c['elapsed_time'] = float(c['max_time_ms'] - c['min_time_ms']) / 1000

	if c['min_resp_ms'] is None or c['min_resp_ms'] > stat[7]:
		c['min_resp_ms'] = stat[7]
	if c['max_resp_ms'] is None or c['max_resp_ms'] < stat[8]:
		c['max_resp_ms'] = stat[8]

	c['total_resp_ms'] += stat[6]

	c['avg_resp_ms'] = int(round(float(c['total_resp_ms']) / c['batch_cnt']))
	c['avg_rows_per_sec'] = int(round(float(c['rows_cnt']) / c['elapsed_time']))

	if c['min_batch_speed'] is None or c['min_batch_speed'] > stat[10]:
		c['min_batch_speed'] = stat[10]
	if c['max_batch_speed'] is None or c['max_batch_speed'] < stat[11]:
		c['max_batch_speed'] = stat[11]

	c['batch_speed_medians'].append(stat[12])

	c['resp_time_medians'].append(stat[9])
	c['speed_medians'].append(int(round(float(stat[5]) / (float(stat[2] - stat[1]) / 1000))))
	c['speeds_per_measure'].append(int(round(float(stat[5]) / (float(stat[3]) / 1000))))

	if len(c['speed_medians']) > 1:
		idx = len(c['speed_medians']) / 2
		c['mid_resp_ms'] = sorted(c['resp_time_medians'])[idx]
		c['mid_batch_speed'] = sorted(c['batch_speed_medians'])[idx]
		c['min_rows_per_sec'] = min(c['speed_medians'])
		c['max_rows_per_sec'] = max(c['speed_medians'])
		c['mid_rows_per_sec'] = sorted(c['speed_medians'])[idx]

		c['min_rows_per_sec2'] = min(c['speeds_per_measure'])
		c['max_rows_per_sec2'] = max(c['speeds_per_measure'])
		c['mid_rows_per_sec2'] = sorted(c['speeds_per_measure'])[idx]


def execute_stats(stats_queue, stop_queue, stats_file_name, stats_sleep_time, stats_debug_info):
	grant_total_batch_cnt = 0
	grant_total_rows_cnt = 0

	results = []
	cur_batch_size = 0
	c = {}

	csv_file = CSVFile(0, 0, stats_file_name)
	csv_file.connect()

	csv_file.insert_row([
		'datetime',
		'm_bath_size',
		'm_min_time', 'm_max_time', 'm_time_delta',
		'm_batch_cnt', 'm_rows_cnt', 'm_total_resp_ms',
		'm_min_resp_ms', 'm_max_resp_ms', 'm_mid_resp_ms',
		'm_min_batch_speed', 'm_max_batch_speed', 'm_mid_batch_speed', 'm_tot_batch_speed',
		'total_batch_cnt', 'total_rows_cnt',
		'cur_batch_size', 'batch_cnt', 'rows_cnt', 'elapsed_time',
		'avg_resp_ms', 'min_resp_ms', 'max_resp_ms', 'mid_resp_ms',
		'min_batch_speed', 'max_batch_speed', 'mid_batch_speed',
		'min_rows_per_sec2', 'max_rows_per_sec2', 'mid_rows_per_sec2',
		'avg_rows_per_sec', 'min_rows_per_sec', 'max_rows_per_sec', 'mid_rows_per_sec'])

	while True:
		if stats_queue.qsize() > 0:
			stat = stats_queue.get()

			# When batch size is changing counters need to be zeroed
			if stat[0] != cur_batch_size:
				if cur_batch_size > 0:
					results.append([
						grant_total_batch_cnt, grant_total_rows_cnt,
						cur_batch_size, c['batch_cnt'], c['rows_cnt'], c['elapsed_time'],
						c['avg_resp_ms'], c['min_resp_ms'], c['max_resp_ms'], c['mid_resp_ms'],
						c['min_batch_speed'], c['max_batch_speed'], c['mid_batch_speed'],
						c['min_rows_per_sec2'], c['max_rows_per_sec2'], c['mid_rows_per_sec2'],
						c['avg_rows_per_sec'], c['min_rows_per_sec'], c['max_rows_per_sec'], c['mid_rows_per_sec']])
				cur_batch_size = stat[0]
				c = get_zero_counters()

			if stat[0] == 0:
				break
			else:
				grant_total_batch_cnt += stat[4]
				grant_total_rows_cnt += stat[5]

				upd_counters(c, stat)

				csv_file.insert_row([datetime.now().strftime("%Y-%m-%d %H:%M:%S")] + stat + [
					grant_total_batch_cnt, grant_total_rows_cnt,
					cur_batch_size, c['batch_cnt'], c['rows_cnt'], c['elapsed_time'],
					c['avg_resp_ms'], c['min_resp_ms'], c['max_resp_ms'], c['mid_resp_ms'],
					c['min_batch_speed'], c['max_batch_speed'], c['mid_batch_speed'],
					c['min_rows_per_sec2'], c['max_rows_per_sec2'], c['mid_rows_per_sec2'],
					c['avg_rows_per_sec'], c['min_rows_per_sec'], c['max_rows_per_sec'], c['mid_rows_per_sec']])

				if stats_debug_info == 1:
					print \
						"Total batches: %d, rows: %d. B.Size: %d, batches: %d, rows: %d, time: %.3f, " \
						"avg/min/max/med{resp: %d / %d / %d / %d, rows/sec(batch|measure|total): N / %d / %d / %d | N / %d / %d / %d | %d / %d / %d / %d}" % (
							grant_total_batch_cnt, grant_total_rows_cnt,
							cur_batch_size, c['batch_cnt'], c['rows_cnt'], c['elapsed_time'],
							c['avg_resp_ms'], c['min_resp_ms'], c['max_resp_ms'], c['mid_resp_ms'],
							stat[10], stat[11], stat[12],
							c['min_rows_per_sec2'], c['max_rows_per_sec2'], c['mid_rows_per_sec2'],
							c['avg_rows_per_sec'], c['min_rows_per_sec'], c['max_rows_per_sec'], c['mid_rows_per_sec'])
		else:
			sleep(stats_sleep_time)
	csv_file.close()

	result_file = stats_file_name.split('.')
	csv_file_res = CSVFile(0, 0, '%s_results.%s' % ('.'.join(result_file[:-1]), result_file[-1:][0]))
	csv_file_res.connect()
	csv_file_res.insert_row([
		'datetime',
		'total_batch_cnt', 'total_rows_cnt',
		'cur_batch_size', 'batch_cnt', 'rows_cnt', 'elapsed_time',
		'avg_resp_ms', 'min_resp_ms', 'max_resp_ms', 'mid_resp_ms',
		'min_batch_speed', 'max_batch_speed', 'mid_batch_speed',
		'min_rows_per_sec2', 'max_rows_per_sec2', 'mid_rows_per_sec2',
		'avg_rows_per_sec', 'min_rows_per_sec', 'max_rows_per_sec', 'mid_rows_per_sec'])
	print "GRAND TOTAL STATISTICS:"
	for r in results:
		csv_file_res.insert_row([datetime.now().strftime("%Y-%m-%d %H:%M:%S")] + r)
		print \
			"Total batches: %d, rows: %d. B.Size: %d, batches: %d, rows: %d, time: %.3f, " \
			"avg/min/max/med{resp: %d / %d / %d / %d, rows/sec(batch|measure|total): N / %d / %d / %d | N / %d / %d / %d | %d / %d / %d / %d}" % (
				r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15], r[16], r[17], r[18], r[19])
	csv_file_res.close()
	stop_queue.put("Statistics thread has been completed.")


def convert_row_values(row):
	values = []
	for i in range(len(row)):
		if row[i] is None or row[i] == '':
			values.append(None)
		elif i == 0 or i == 5 or i == 6 or i == 12 or i == 13:
			values.append(int(row[i]))
		else:
			values.append(row[i])
	return values


def read_csv(csv_file, batch_size_steps, batch_sizes):
	rows = []
	cur_step = 0
	cur_batch_cnt = 0
	wait_trans = 0
	f = open(csv_file, "rb")
	csv_reader = csv.reader(f, delimiter=',', quotechar='"')
	for row in csv_reader:
		rows.append(convert_row_values(row))
		if cur_step == len(batch_sizes):
			cur_step -= 1
			del rows[:]
			break
		if len(rows) == batch_sizes[cur_step]:
			cur_batch_cnt += 1
			if batch_size_steps[cur_step] > 0:
				if batch_size_steps[cur_step] == cur_batch_cnt:
					cur_batch_cnt = 0
					cur_step += 1
					wait_trans = 1
			yield [wait_trans, cur_step, rows]
			rows = []
			wait_trans = 0

	if len(rows) > 0:
		yield [wait_trans, cur_step, rows]
	f.close()


def execute_reader(data_queue, pause_reader_queue, pause_workers_queue, csv_file_name, worker_count, batch_size_steps, batch_sizes):
	batch_cnt = 0
	total_rows = 0
	csv_iterator = read_csv(csv_file_name, batch_size_steps, batch_sizes)
	start_time = int(round(time() * 1000))
	while True:
		try:
			if pause_reader_queue.qsize() == 0:
				rows = csv_iterator.next()
				wait_trans = rows[0]
				cur_step = rows[1]
				total_rows += len(rows[2])
				data_queue.put([wait_trans, rows[2]])
				batch_cnt += 1
				if wait_trans == 1:
					print 'Reader has reached batch size change %d rows were read in batches: %d in %.3f sec. queue.qsize: %d' % (total_rows, batch_cnt, float(int(round(time() * 1000)) - start_time) / 1000, data_queue.qsize())
					sleep(2)
					pause_reader_queue.put(1)
					s = pause_workers_queue.get()
				if cur_step == len(batch_sizes):
					break
			else:
				sleep(1)
				start_time = int(round(time() * 1000))
				batch_cnt = 0
				total_rows = 0
		except StopIteration:
			break
	for worker in range(worker_count):
		data_queue.put([0, []])
	s = pause_workers_queue.get()
	print "Reader thread has been completed. Processed batch count is %d." % batch_cnt


def build_worker_pool(
		data_queue, resp_queue, stats_queue, stop_queue, pause_reader_queue, pause_workers_queue,
		csv_file_name,
		worker_count, batch_size_steps, batch_sizes,
		stats_file_name, stats_sleep_time, stats_debug_info,
		db_type, csv_db_file_name,
		delay_ms):

	workers = []
	worker = Process(target=execute_reader, args=(data_queue, pause_reader_queue, pause_workers_queue, csv_file_name, worker_count, batch_size_steps, batch_sizes))
	workers.append(worker)
	worker.start()

	worker = Process(target=execute_aggregate, args=(delay_ms, resp_queue, stats_queue, pause_reader_queue, worker_count))
	workers.append(worker)
	worker.start()

	worker = Process(target=execute_stats, args=(stats_queue, stop_queue, stats_file_name, stats_sleep_time, stats_debug_info))
	workers.append(worker)
	worker.start()

	for worker_id in range(worker_count):
		if db_type == CSV_DB:
			output_file_name = '%s_%d%s' % (csv_db_file_name[0:-4], worker_id, csv_db_file_name[len(csv_db_file_name)-4:])
		else:
			output_file_name = None
		worker = Process(target=execute, args=(data_queue, resp_queue, pause_workers_queue, worker_id, db_type, output_file_name))
		workers.append(worker)
		worker.start()

	return workers


def main():
	prm = get_args()

	start_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	print "%s> Go" % start_time

	data_queue = Queue()
	resp_queue = Queue()
	stats_queue = Queue()
	stop_queue = Queue()
	pause_workers_queue = Queue()
	pause_reader_queue = Queue()
	pause_workers_queue.put(1)

	worker_threads = build_worker_pool(
		data_queue, resp_queue, stats_queue, stop_queue, pause_reader_queue, pause_workers_queue,
		prm['csv_file_name'],
		prm['worker_count'], prm['batch_size_steps'], prm['batch_sizes'],
		prm['stats_file_name'], prm['stats_sleep_time'], prm['stats_debug_info'],
		prm['db_type'], prm['csv_db_file_name'], prm['delay_ms'])

	while stop_queue.qsize() == 0:
		sleep(5)

	print stop_queue.get()
	print "Main thread has been completed. Start time: %s, End time: %s" % (start_time, datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	exit(1)


def get_args():
	try:
		if len(sys.argv) == 1:
			print_usage()
			exit(1)

		csv_file_name = sys.argv[1]
		if csv_file_name is None or csv_file_name == '':
			raise ValueError('Value "%s" is incorrect. {csv_file_name} must be supplied.' % csv_file_name)

		if not os.path.isfile(csv_file_name):
			raise ValueError('File "%s" does not exist.' % csv_file_name)

		worker_count = int(sys.argv[2])
		if worker_count < 1 or worker_count > 100:
			raise ValueError('Value %d is incorrect. {worker_count} can take values only between 1 and 100.' % worker_count)

		batch_size_steps = []
		batch_size_steps_arg = sys.argv[3]
		for val in batch_size_steps_arg.split(','):
			batch_size_steps.append(int(val))
		if len(batch_size_steps) == 0:
			raise ValueError('Value %s is incorrect. {batch_sizes} can take values only between 1 and 1000.' % batch_size_steps_arg)

		batch_sizes = []
		batch_sizes_arg = sys.argv[4]
		for val in batch_sizes_arg.split(','):
			if 0 < int(val) < 1001:
				batch_sizes.append(int(val))
		if len(batch_sizes) == 0:
			raise ValueError('Value %s is incorrect. {batch_sizes} can take values only between 1 and 1000.' % batch_sizes_arg)

		if len(batch_size_steps) != len(batch_sizes):
			raise ValueError('Count of {batch_size_steps}="%s" must much {batch_sizes}="%s".' % (batch_size_steps, batch_sizes))

		stats_file_name = sys.argv[5]
		if stats_file_name is None or stats_file_name == '':
			raise ValueError('Value "%s" is incorrect. {stats_file_name} must be supplied.' % stats_file_name)

		stats_sleep_time = float(sys.argv[6])
		if stats_sleep_time < 0 or stats_sleep_time > 10:
			raise ValueError('Value %.3f is incorrect. {stats_sleep_time} can take values only between 0 and 9.9 (sec.)' % stats_sleep_time)

		stats_debug_info = int(sys.argv[7])
		if stats_debug_info < 0 or stats_debug_info > 1:
			raise ValueError('Value %d is incorrect. {stats_debug_info} only can take value 0 or 1.' % stats_debug_info)

		db_type = int(sys.argv[8])
		if db_type < 0 or db_type > 7:
			raise ValueError('Value %d is incorrect. {db_type} only can take value 1 for IBM DB2, 2 for MS SQL, 3 for CSV File, 4 for ClickHouse or 5 for HP Vertica or 6 for MariaDB or 7 for MySQLDB;' % db_type)

		csv_db_file_name = sys.argv[9]
		if db_type == 3 and (csv_db_file_name is None or csv_db_file_name == ''):
			raise ValueError('Value "%s" is incorrect. {csv_db_file_name} must be supplied if {db_type}=3.' % csv_db_file_name)

		delay_ms = int(sys.argv[10])
		if delay_ms < 100 or db_type > 10000:
			raise ValueError('Value %d is incorrect. {delay_ms} only can take value 0 or 1.' % delay_ms)

		print "%s> Settings: \n" \
			'\tcsv_file_name: "%s", \n' \
			'\tworker_count: %d, batch_size_steps: %s, batch_sizes: %s, \n' \
			'\tstats_file_name: "%s", stats_sleep_time: %.3f, stats_debug_info: %d, \n' \
			'\tdb_type: %d, csv_db_file_name: "%s", \n' \
			'\tdelay_ms: %d,\n' % (
				datetime.now().strftime("%Y-%m-%d %H:%M:%S"), csv_file_name,
				worker_count, batch_size_steps, batch_sizes,
				stats_file_name, stats_sleep_time, stats_debug_info,
				db_type, csv_db_file_name, delay_ms)

		sleep(2)

		return {
			'csv_file_name': csv_file_name,
			'worker_count': worker_count, 'batch_size_steps': batch_size_steps, 'batch_sizes': batch_sizes,
			'stats_file_name': stats_file_name, 'stats_sleep_time': stats_sleep_time, 'stats_debug_info': stats_debug_info,
			'db_type': db_type, 'csv_db_file_name': csv_db_file_name, 'delay_ms': delay_ms-1}

	except Exception as ex:
		print "\nArgument error: %s. %s\n" % (ex.__class__.__name__, str(ex))
		print_usage()
		exit(1)


def print_usage():
	usage_str = \
		'python run_batch_insert.pyc {csv_file_name} \\\n' \
		'\t{worker_count} {batch_size_step} {batch_size} \\\n' \
		'\t{stats_file_name} {stats_sleep_time} {stats_debug_info} \\\n' \
		'\t{db_type} {csv_db_file_name}'
	example_str = 'python run_batch_insert.pyc "\\tmp\\access_log.csv" 8 500 1 100 0.5 1 "\\tmp\\load_statistics.csv" 2 1 2 ""'

	usage_restrictions = \
		'\t{csv_file_name} must be supplied file path to the access log; \n' \
		'\t{worker_count} can take values only between 1 and 100; \n' \
		'\t{batch_size_steps} the step to change batch size. Can take values only between -10000000 to 10000000, in 0 case step batch_size is constant; \n' \
		'\t{batch_sizes} can take values only between 1 and 1000; \n' \
		'\t{stats_file_name} must be supplied file path where to store statistics info; \n' \
		'\t{stats_sleep_time} can take values only between 0 and 9.9 (sec.); \n' \
		'\t{stats_debug_info} only can take value 0 or 1; \n' \
		'\t{db_type} only can take value 1 for IBM DB2, 2 for MS SQL, 3 for CSV File, 4 for ClickHouse or 5 for HP Vertica; \n' \
		'\t{csv_db_file_name} must be supplied file path which will be used as destination csv file for write sub processes. \n\t\t' \
		'Used only for {db_type}=3. For each sub process will be created separate file by adding process id index as file name suffix.'

	print 'Usage:\n%s\nExample:\n%s\nRestrictions:\n%s' % (usage_str, example_str, usage_restrictions)

if __name__ == "__main__":
	log_file_name = '%s/%s.%s' % \
		(os.path.dirname(os.path.abspath(__file__)), os.path.basename(__file__).split('.')[:-1][0], 'log')
	logging.basicConfig(
		level=logging.DEBUG,
		filename=log_file_name,
		filemode='a', datefmt='%Y-%m-%d %H:%M:%S',
		format='%(asctime)-15s> %(name)s %(levelname)s %(message)s')
	main()
