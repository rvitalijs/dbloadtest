# -*- coding: utf-8 -*-

import csv
import ibm_db
import pymssql
import requests
# import hp_vertica_client
import vertica_python
import mysql.connector
from core import LogExtension
from db import IBM_DB2, CSV_DB, MS_SQL, CLICK_HOUSE, HP_VERTICA, MARIA_DB, MYSQL_DB


class DB(object):
	def connect(self):
		pass

	def autocommit(self, ac):
		pass

	def commit(self, ac):
		pass

	@staticmethod
	def rows2sql_values(rows):
		sql_values = ''
		for row in rows:
			sql_values += ', (%s)' % DB.row2sql_values(row)
		return sql_values[2:]

	@staticmethod
	def row2sql_values(row):
		sql_values = ''
		for value in row:
			if value is None:
				sql_values += ', NULL'
			elif isinstance(value, basestring):
				sql_values += ', \'%s\'' % value.replace("'", "''")
			elif isinstance(value, int) or isinstance(value, float):
				sql_values += ', %s' % value
			elif isinstance(value, unicode):
				sql_values += ', \'%s\'' % value.replace("'", "''")
			else:
				sql_values += ', NULL'
		return sql_values[2:]

	@staticmethod
	def rows2sql_values_t(rows):
		sql_values = ()
		for row in rows:
			sql_values += (DB.row2sql_values_t(row),)
		return sql_values

	@staticmethod
	def rows2sql_values_l(rows):
		sql_values = []
		for row in rows:
			sql_values.append(DB.row2sql_values_t(row))
		return sql_values

	@staticmethod
	def row2sql_values_t(row):
		sql_values = ()
		for value in row:
			if value is None:
				sql_values += (None,)
			elif isinstance(value, basestring):
				sql_values += ('%s' % value.replace("'", "''"),)
			elif isinstance(value, int) or isinstance(value, float):
				sql_values += (value,)
			elif isinstance(value, unicode):
				sql_values += ('%s' % value.replace("'", "''"),)
			else:
				sql_values += (None,)
		return sql_values

	@staticmethod
	def rows2sql_values_ch(rows):
		sql_values = ''
		for row in rows:
			sql_values += ', (%s)' % DB.row2sql_values_ch(row)
		return sql_values[2:]

	@staticmethod
	def row2sql_values_ch(row):
		sql_values = ''
		val_no = 0
		for value in row:
			if value is None:
				if val_no in [0, 5, 6, 12, 13]:
					sql_values += ', 0'
				else:
					sql_values += ', \'\''
			elif isinstance(value, basestring):
				sql_values += ', \'%s\'' % value.replace("'", "''")
			elif isinstance(value, int) or isinstance(value, float):
				sql_values += ', %s' % value
			elif isinstance(value, unicode):
				sql_values += ', \'%s\'' % value.replace("'", "''")
			else:
				sql_values += ', NULL'
			val_no += 1
		return sql_values[2:]

	@staticmethod
	def rows2sql_values_v(rows):
		sql_values = []
		for row in rows:
			sql_values.append(DB.row2sql_values_v(row))
		return sql_values

	@staticmethod
	def row2sql_values_v(row):
		sql_values = ()
		val_no = 0
		for value in row:
			if value is None:
				if val_no in [0, 5, 6, 12, 13]:
					sql_values += (0,)
				else:
					sql_values += ('',)
			elif isinstance(value, basestring):
				sql_values += ('%s' % value.replace("'", "''"),)
			elif isinstance(value, int) or isinstance(value, float):
				sql_values += (value,)
			elif isinstance(value, unicode):
				sql_values += ('%s' % value.replace("'", "''"),)
			else:
				sql_values += (None,)
			val_no += 1
		return sql_values


class VerticaDB(DB):
	def __init__(self, worker_id, task_no):
		self.__worker_id = worker_id
		self.__task_no = task_no
		self.__conn = None

	def connect(self):
		try:
			conn = {
				'host': '10.50.50.177', 'port': 5433,
				'user': 'dbadmin', 'password': 'vertica', 'database': 'testdb',
				'read_timeout': 600, 'unicode_error': 'strict', 'ssl': False, 'connection_timeout': 5}
			self.__conn = vertica_python.connect(**conn)
			# self.__conn = hp_vertica_client.connect(host="10.50.50.177", database="testdb", user='dbadmin', password='vertica')
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s." % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	def autocommit(self, ac):
		try:
			cur = self.__conn.cursor()
			if ac:
				cur.execute("SET SESSION AUTOCOMMIT TO on;")
			else:
				cur.execute("SET SESSION AUTOCOMMIT TO off;")
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	def insert_access_log_rows(self, rows):
		try:
			cur = self.__conn.cursor()
			if len(rows) > 1:
				with open('temp_%d.csv' % self.__worker_id, 'w') as f:
					writer = csv.writer(f, delimiter=',', quoting=csv.QUOTE_NONE, quotechar='', escapechar='', lineterminator='\n')
					for row in rows:
						writer.writerow(row)
				q = "copy TEST.ACCESS_LOG(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) " \
					"from stdin delimiter ',' "
				with open('temp_%d.csv' % self.__worker_id, 'rb') as f:
					cur.copy(q, f)
			else:
				sql_insert = \
					'INSERT INTO TEST.ACCESS_LOG(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) ' \
					'VALUES %s' % DB.rows2sql_values(rows)
				cur.execute(sql_insert)
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s." % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	# def insert_access_log_rows(self, rows):
	# 	try:
	# 		cur = self.__conn.cursor()
	# 		if len(rows) == 1:
	# 			sql_insert = \
	# 				'INSERT INTO TEST.ACCESS_LOG2(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) ' \
	# 				'VALUES (%s)' % DB.row2sql_values(rows[0])
	# 			cur.execute(sql_insert)
	# 		else:
	# 			sql_insert = \
	# 				"INSERT INTO TEST.ACCESS_LOG2(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) " \
	# 				"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
	# 			cur.executemany(sql_insert, DB.rows2sql_values_v(rows))
	# 	except Exception as ex:
	# 		err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s." % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
	# 		raise Exception(err_str.replace("\r", "\n"))

	def close(self):
		self.__conn.close()


class CSVFile(DB):
	def __init__(self, worker_id, task_no, output_file_name, delimiter=',', quoting=csv.QUOTE_NONE, quotechar='', escapechar=''):
		self.__worker_id = worker_id
		self.__task_no = task_no
		self.__output_file_name = output_file_name
		self.__csv_file = None
		self.__csv_writer = None
		self.__delimiter = delimiter
		self.__quoting = quoting
		self.__quotechar = quotechar
		self.__escapechar = escapechar

	def connect(self):
		self.__csv_file = open(self.__output_file_name, 'ab')
		self.__csv_writer = csv.writer(self.__csv_file, delimiter=self.__delimiter, quoting=self.__quoting, quotechar=self.__quotechar, escapechar=self.__escapechar)

	def insert_row(self, row_data):
		self.__csv_writer.writerow(row_data)

	def insert_rows(self, rows):
		self.insert_access_log_rows(rows)

	def insert_access_log_rows(self, rows):
		for row in rows:
			self.__csv_writer.writerow(row)

	def close(self):
		self.__csv_file.close()


class IbmDb2DB(DB):
	def __init__(self, worker_id, task_no):
		self.__worker_id = worker_id
		self.__task_no = task_no
		self.__conn = None
		self.__cnt = 0

	def connect(self):
		try:
			self.__conn = ibm_db.connect("DATABASE=testdb2;HOSTNAME=10.50.50.150;PORT=50000;PROTOCOL=TCPIP;UID=db2inst1;PWD=db2-16;", "", "")
		except:
			err_str = "Worker(%d) task no %d, ibm_db: %s" % (self.__worker_id, self.__task_no, ibm_db.conn_errormsg())
			raise Exception(err_str.replace("\r", "\n"))

	def insert_access_log_row(self, row):
		self.exec_sql(
			'INSERT INTO TEST.ACCESS_LOG(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) '
			'VALUES %s' % DB.row2sql_values(row))

	def insert_access_log_rows(self, rows):
		try:
			sql_insert = \
				"INSERT INTO TEST.ACCESS_LOG(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) " \
				"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
			stmt = ibm_db.prepare(self.__conn, sql_insert)
			ibm_db.execute_many(stmt, DB.rows2sql_values_t(rows))
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while inserting data: %s.\nSQL: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex), ibm_db.stmt_errormsg())
			raise Exception(err_str.replace("\r", "\n"))

	def exec_sql(self, sql):
		try:
			stmt = ibm_db.prepare(self.__conn, sql)
			ibm_db.execute(stmt)
			# stmt = ibm_db.exec_immediate(self.__conn, sql)
			# n = ibm_db.num_rows(stmt)
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while inserting data: %s.\nSQL: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex), ibm_db.stmt_errormsg())
			raise Exception(err_str.replace("\r", "\n"))

	def close(self):
		ibm_db.close(self.__conn)


class ClickHouse(DB):
	def __init__(self, worker_id, task_no):
		self.__worker_id = worker_id
		self.__task_no = task_no
		self.__url = 'http://10.50.50.36:8123/'

	def connect(self):
		pass

	def insert_access_log_row(self, row):
		self.exec_sql(
			'INSERT INTO testdb.access_log(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) '
			'VALUES %s' % DB.row2sql_values_ch(row))

	def insert_access_log_rows(self, rows):
		self.exec_sql(
			'INSERT INTO testdb.access_log(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) '
			'VALUES %s' % DB.rows2sql_values_ch(rows))

	def exec_sql(self, sql):
		try:
			headers = {'Content-type': 'text/plain; charset=UTF-8', 'Connection': 'close'}
			response = requests.post(self.__url, data=sql, headers=headers)
			if response.status_code != 200:
				raise Exception("Response code: %d, Content: %s" % (response.status_code, response.content))
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while inserting data: %s." % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	def close(self):
		pass


class MySQLDB(object):
	def __init__(self, worker_id, task_no):
		self.__worker_id = worker_id
		self.__task_no = task_no
		self.__conn = None
		self.__cnt = 0

	def connect(self):
		try:
			self.__conn = mysql.connector.connect(user='testuser', password='infinidb', host='192.168.1.226', database='TestDB')
			self.__conn
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	def autocommit(self, ac):
		try:
			# self.__conn.autocommit = ac
			a = ac
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	def commit(self):
		try:
			self.__conn.commit()
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	def insert_access_log_row(self, row):
		self.exec_sql(
			'INSERT INTO ACCESS_LOG(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) '
			'VALUES %s' % DB.row2sql_values(row))
		self.__conn.commit()

	def insert_access_log_rows(self, rows):
		self.exec_sql(
			'INSERT INTO ACCESS_LOG(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) '
			'VALUES %s' % DB.rows2sql_values(rows))
		self.__conn.commit()

	def exec_sql(self, sql):
		try:
			cursor = self.__conn.cursor()
			cursor.execute(sql)
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while inserting data: %s.\nSQL: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex), '')  #sql
			raise Exception(err_str.replace("\r", "\n"))

	def close(self):
		self.__conn.close()


class MariaDB(object):
	def __init__(self, worker_id, task_no):
		self.__worker_id = worker_id
		self.__task_no = task_no
		self.__conn = None
		self.__cnt = 0

	def connect(self):
		try:
			self.__conn = mysql.connector.connect(user='root', password='mariadb', host='10.50.50.190', database='TestDB')
			self.__conn
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	def autocommit(self, ac):
		try:
			self.__conn.autocommit = False
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	def commit(self):
		try:
			self.__conn.commit()
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	def insert_access_log_row(self, row):
		self.exec_sql(
			'INSERT INTO ACCESS_LOG(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) '
			'VALUES %s' % DB.row2sql_values(row))

	def insert_access_log_rows(self, rows):
		self.exec_sql(
			'INSERT INTO ACCESS_LOG(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) '
			'VALUES %s' % DB.rows2sql_values(rows))
		self.__conn.commit()

	def exec_sql(self, sql):
		try:
			cursor = self.__conn.cursor()
			cursor.execute(sql)
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while inserting data: %s.\nSQL: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex), '')  #sql
			raise Exception(err_str.replace("\r", "\n"))

	def close(self):
		self.__conn.close()


class MsSqlDB(object):
	def __init__(self, worker_id, task_no):
		self.__worker_id = worker_id
		self.__task_no = task_no
		self.__conn = None
		self.__cnt = 0

	def connect(self):
		try:
			self.__conn = pymssql.connect(server='10.50.50.10', user='sa', password='WinSrv16', database='TestDB')
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	def autocommit(self, ac):
		try:
			self.__conn.autocommit(ac)
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	def commit(self):
		try:
			self.__conn.commit()
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while connecting: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex))
			raise Exception(err_str.replace("\r", "\n"))

	def insert_access_log_row(self, row):
		self.exec_sql(
			'INSERT INTO [dbo].[ACCESS_LOG]([ID],[IPADDRESS],[CLIENTID],[USERID],[REQDATE],[REQTIMESTAMP],[REQTIME],[METHOD],[ENDPOINT],[PROTOCOL],[REFERRER],[USERAGENT],[RESPONSECODE],[CONTENTSIZE]) '
			'VALUES %s' % DB.row2sql_values(row)) # SET IDENTITY_INSERT [dbo].[ACCESS_LOG] ON; SET IDENTITY_INSERT [dbo].[ACCESS_LOG] OFF;

	def insert_access_log_rows(self, rows):
		# if self.__cnt < 3000:
		# 	sql_insert = \
		# 		"INSERT INTO dbo.ACCESS_LOG(ID,IPADDRESS,CLIENTID,USERID,REQDATE,REQTIMESTAMP,REQTIME,METHOD,ENDPOINT,PROTOCOL,REFERRER,USERAGENT,RESPONSECODE,CONTENTSIZE) " \
		# 		"VALUES(%d, %s, %s, %s, %s, %d, %d, %s, %s, %s, %s, %s, %d, %d)"
		# 	cursor = self.__conn.cursor()
		# 	cursor.executemany(sql_insert, DB.rows2sql_values_l(rows))
		# else:
		self.exec_sql(
			'INSERT INTO [dbo].[ACCESS_LOG]([ID],[IPADDRESS],[CLIENTID],[USERID],[REQDATE],[REQTIMESTAMP],[REQTIME],[METHOD],[ENDPOINT],[PROTOCOL],[REFERRER],[USERAGENT],[RESPONSECODE],[CONTENTSIZE]) '
			'VALUES %s' % DB.rows2sql_values(rows))
		# self.__cnt += 1

	def exec_sql(self, sql):
		try:
			cursor = self.__conn.cursor()
			cursor.execute(sql)
		except Exception as ex:
			err_str = "Worker(%d) task no %d, Exception(%s) raised while inserting data: %s.\nSQL: %s" % (self.__worker_id, self.__task_no, ex.__class__.__name__, str(ex), '')  #sql
			raise Exception(err_str.replace("\r", "\n"))

	def close(self):
		self.__conn.close()


class DBHandler(LogExtension):
	def __init__(self, worker_id, task_no, db_type, csv_db_output_file_name=None):
		self.__worker_id = worker_id
		self.__task_no = task_no
		self.__db_type = db_type
		self.__csv_db_output_file_name = csv_db_output_file_name
		self.__db = None

	def db(self):
		if self.__db is None:
			if self.__db_type == CSV_DB:
				self.__db = CSVFile(self.__worker_id, self.__task_no, self.__csv_db_output_file_name)
			elif self.__db_type == IBM_DB2:
				self.__db = IbmDb2DB(self.__worker_id, self.__task_no)
			elif self.__db_type == MS_SQL:
				self.__db = MsSqlDB(self.__worker_id, self.__task_no)
			elif self.__db_type == CLICK_HOUSE:
				self.__db = ClickHouse(self.__worker_id, self.__task_no)
			elif self.__db_type == HP_VERTICA:
				self.__db = VerticaDB(self.__worker_id, self.__task_no)
			elif self.__db_type == MARIA_DB:
				self.__db = MariaDB(self.__worker_id, self.__task_no)
			elif self.__db_type == MYSQL_DB:
				self.__db = MySQLDB(self.__worker_id, self.__task_no)
		return self.__db
