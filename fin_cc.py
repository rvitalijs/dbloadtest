# -*- coding: utf-8 -*-

import logging
import os
import sys
import csv

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def main():
	csv_file = 'columns-csv-out.txt'

	x = 0
	strrr = ""
	sqlVals = ""
	sqlVals2 = ""
	f = open('columns-csv-2014.txt', "rb")
	csv_reader = csv.reader(f, delimiter=',', quotechar='"')
	for row in csv_reader:
		a = [unicode(cell, 'utf-8').encode('utf-8').strip() for cell in row]
		print "[Column %d] AS [%s], " % (x, a[1])
		strrr += "[%s], " % a[1]
		sqlVals += "(%s), " % a[1]
		sqlVals2 += "ISNULL([%s], 0) AS [%s], " % (a[1], a[1])
		x += 1

	print strrr
	print sqlVals
	print sqlVals2
	exit(1)


if __name__ == "__main__":
	log_file_name = '%s/%s.%s' % \
		(os.path.dirname(os.path.abspath(__file__)), os.path.basename(__file__).split('.')[:-1][0], 'log')
	logging.basicConfig(
		level=logging.DEBUG,
		filename=log_file_name,
		filemode='a', datefmt='%Y-%m-%d %H:%M:%S',
		format='%(asctime)-15s> %(name)s %(levelname)s %(message)s')
	main()
