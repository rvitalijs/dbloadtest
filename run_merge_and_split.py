# -*- coding: utf-8 -*-

import logging
import os
import sys
from datetime import datetime
import csv
from multiprocessing import Process, Queue
from time import sleep, time

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from db.db_handler import CSVFile
from core.helpers.file_helper import FileHelper


def execute(data_queue, stop_queue, output_file_name, output_file_count):
	total_rows = 0

	out_files = []
	for file_id in range(output_file_count):
		output_file_name_tmp = '%s_%d%s' % (output_file_name[0:-4], file_id, output_file_name[len(output_file_name) - 4:])
		out_files.append(CSVFile(file_id, file_id, output_file_name_tmp))
		out_files[len(out_files)-1].connect()
	while True:
		if data_queue.qsize() > 0:
			row = data_queue.get()
			if len(row) == 0:
				break
			out_files[total_rows % output_file_count].insert_row(row)
			total_rows += 1
		else:
			sleep(0.1)

	for file_id in range(output_file_count):
		out_files[file_id].close()
		print "Writer has completed file %d ." % file_id

	stop_queue.put("Writer has completed files %d ." % output_file_count)


def execute_reader(file_list, data_queue, csv_path, start_row_no, output_file_count):
	row_no = start_row_no
	for fn in file_list:
		csv_file = '%s\%s' % (csv_path, fn)
		f = open('%s\%s' % (csv_path, fn), "rb")
		csv_reader = csv.reader(f, delimiter=',', quotechar='"')
		for row in csv_reader:
			data_queue.put([row_no] + row[1:])
			row_no += 1
			if data_queue.qsize() > 1000:
				sleep(1)
		print "Reader has completed file %s ." % csv_file
	for worker_id in range(output_file_count):
		data_queue.put([])


def build_worker_pool(file_list, data_queue, stop_queue, csv_path, output_file_count, output_file_name, starting_row_no):
	workers = []
	worker = Process(target=execute_reader, args=(file_list, data_queue, csv_path, starting_row_no, output_file_count))
	workers.append(worker)
	worker.start()

	worker = Process(target=execute, args=(data_queue, stop_queue, output_file_name, output_file_count))
	workers.append(worker)
	worker.start()

	return workers


def main():
	prm = get_args()

	start_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	print "%s> Go" % start_time

	data_queue = Queue()
	stop_queue = Queue()

	# Add the urls to process
	file_list = FileHelper.get_file_list(prm['csv_path'], None, False)

	worker_threads = build_worker_pool(file_list, data_queue, stop_queue, prm['csv_path'], prm['output_file_count'], prm['output_file_name'], prm['starting_row_no'])

	while stop_queue.qsize() < 2:
		sleep(5)

	print stop_queue.get()
	print "Main thread has been completed. Start time: %s, End time: %s" % (start_time, datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
	exit(1)


def get_args():
	try:
		if len(sys.argv) == 1:
			print_usage()
			exit(1)

		csv_path = sys.argv[1]
		if csv_path is None or csv_path == '' or not os.path.isdir(csv_path):
			raise ValueError('{csv_path}: Value "%s" is incorrect or path does not exist.' % csv_path)

		output_file_count = int(sys.argv[2])
		if output_file_count < 1 or output_file_count > 100:
			raise ValueError('Value %d is incorrect. {output_file_count} can take values only between 1 and 100.' % output_file_count)

		output_file_name = sys.argv[3]
		if output_file_name is None or output_file_name == '':
			raise ValueError('Value "%s" is incorrect. {output_file_name} must be supplied.' % output_file_name)

		starting_row_no = int(sys.argv[4])
		if starting_row_no < 1:
			raise ValueError('Value %d is incorrect. {starting_row_no} can be > 1.' % starting_row_no)

		print '%s> Settings: {csv_path}: "%s", {output_file_count}: %d, {output_file_name}: %s, {starting_row_no}: %d' % (
				datetime.now().strftime("%Y-%m-%d %H:%M:%S"), csv_path, output_file_count, output_file_name, starting_row_no)

		sleep(2)

		return {'csv_path': csv_path, 'output_file_count': output_file_count, 'output_file_name': output_file_name, 'starting_row_no': starting_row_no}

	except Exception as ex:
		print "\nArgument error: %s. %s\n" % (ex.__class__.__name__, str(ex))
		print_usage()
		exit(1)


def print_usage():
	usage_str = \
		'python run_batch_insert.pyc {csv_path} {output_file_count} {output_file_name} {starting_row_no}'

	example_str = 'python run_batch_insert.pyc "\\tmp\\access_log.csv" 8 500 1 100 0.5 1 "\\tmp\\load_statistics.csv" 2 1 2 ""'

	usage_restrictions = ''

	print 'Usage:\n%s\nExample:\n%s\nRestrictions:\n%s' % (usage_str, example_str, usage_restrictions)

if __name__ == "__main__":
	log_file_name = '%s/%s.%s' % \
		(os.path.dirname(os.path.abspath(__file__)), os.path.basename(__file__).split('.')[:-1][0], 'log')
	logging.basicConfig(
		level=logging.DEBUG,
		filename=log_file_name,
		filemode='a', datefmt='%Y-%m-%d %H:%M:%S',
		format='%(asctime)-15s> %(name)s %(levelname)s %(message)s')
	main()
